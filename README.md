Er det ikke irriterende at det finnes X antall el-sparkesykler og for hver gang du kommer over en ny type må du laste ned en ny app og legge inn kortinformasjonen din på nytt? Denne irritasjonen er utgangspunktet for ideen i dette prosjektet.

I september 2021 kom Bergen på el-sparkesykkeltoppen i Europa [1]. Dette vil selvsagt føre til at flere aktører ønsker å være en del av dette markedet. Ifølge konkurransetilsynet  kan dette føre til en billigere tjeneste for brukerne [2], og dette er jo bra for kundene. Men for samtidig å unngå unødig irritasjon og merarbeid for kundene  mener vi at det er et marked for å  opprette en organisasjon og plattform som samler alle aktørene i en tjeneste og app slik at brukerne kan forholder seg til kun en app og betalingsplattform på sin mobiltelefon.

